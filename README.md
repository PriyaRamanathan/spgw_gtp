# README #

GTP interface set up and read the SPGW configuration file from OAI
Read the gtp tunnel information of the currently active SPGW container
Update the tunnel information on the destination node once the migration is initiated
Script to read the tunnel list and keep a local copy.

### What is this repository for? ###

* SPGW container migration support.


### How do I get set up? ###

libgtpnl setup with the gtp-link and gtp-tunnel update.
gtp-link for the gtp interface setup 
gtp-tunnel for reading and updating the tunnel list information
migrator.sh reads the active gtp tunnel information
gtp-tunnel-add.sh to update the tunnel list

### What services would be available?

* gtp-link - To create the gtp device specific interface with the linux kernel
* gtp-tunnel-list - To view the current active gtp tunnels at the Source node
* gtp-tunnel-add - To add the active gtp tunnels at the Destination node




